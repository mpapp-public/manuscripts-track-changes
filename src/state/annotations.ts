/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Mapping, Transform } from 'prosemirror-transform'
import tinycolor from 'tinycolor2'

export interface Annotation {
  uid: string
  from: number
  to: number
  ancestorFrom: number
  ancestorTo: number
  color: [number, number, number]
  updatedAt: number
}

export interface AnnotationSpec
  extends Omit<Annotation, 'ancestorFrom' | 'ancestorTo' | 'color'> {
  color: [number, number, number] | string
}

const convertColor = (
  input: [number, number, number] | string
): [number, number, number] => {
  if (typeof input === 'string') {
    const { r, g, b } = tinycolor(input).toRgb()
    return [r, g, b]
  }
  return input
}

export const loadAnnotations = (
  annotations: Array<Omit<Annotation, 'from' | 'to'>>,
  forwardMapping?: Mapping
): Annotation[] => {
  if (!forwardMapping) {
    return loadAnnotations(annotations, new Mapping())
  }

  return annotations.map((ann) => ({
    ...ann,
    from: forwardMapping.map(ann.ancestorFrom, -1),
    to: forwardMapping.map(ann.ancestorTo, 1),
  }))
}

export const updateAnnotations = (
  annotations: Annotation[],
  tr: Transform
): Annotation[] => {
  return annotations.map((ann) => ({
    ...ann,
    from: tr.mapping.map(ann.from, -1),
    to: tr.mapping.map(ann.to, 1),
  }))
}

export const addAnnotation = (
  initialAnnotations: Annotation[],
  newAnnotation: AnnotationSpec,
  reverseMapping: Mapping
): Annotation[] => {
  return initialAnnotations.concat({
    ...newAnnotation,
    color: convertColor(newAnnotation.color),
    ancestorFrom: reverseMapping.map(newAnnotation.from, -1),
    ancestorTo: reverseMapping.map(newAnnotation.to, 1),
  })
}

export const removeAnnotation = (
  initialAnnotations: Annotation[],
  uid: string
): Annotation[] => {
  return initialAnnotations.filter((ann) => ann.uid !== uid)
}
