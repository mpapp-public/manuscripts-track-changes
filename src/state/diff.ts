/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Mapping, Step, Transform } from 'prosemirror-transform'

import { Commit, smoosh } from './commit'

type Replacements = Array<{
  ancestorFrom: number
  ancestorTo: number
  pos?: number
}>

export interface Diff {
  replacements: Replacements
  forwardMapping: Mapping
  reverseMapping: Mapping
}

export const emptyDiff = (): Diff => ({
  forwardMapping: new Mapping(),
  reverseMapping: new Mapping(),
  replacements: [],
})

const updateDiffInner = (
  { replacements, forwardMapping: fMapping, reverseMapping: rMapping }: Diff,
  steps: Step[],
  mapping: Mapping
) => {
  fMapping.appendMapping(mapping)

  // remap all the existing replacement points through just the new Steps
  const remappedOldPoints = replacements.map((point) => ({
    ...point,
    pos: point.pos && mapping.map(point.pos),
  }))

  // find new Replacement points and add to the reverse mapping
  const rawSpans = steps.reduce((acc, step) => {
    const map = step.getMap()

    map.forEach((oldStart, oldEnd) => {
      const ancestorFrom = rMapping.map(oldStart)
      const point = {
        ancestorFrom,
        ancestorTo: rMapping.map(oldEnd),
        pos: fMapping.map(ancestorFrom),
      }
      if (point.ancestorTo <= point.ancestorFrom) {
        return
      }
      acc.push(point)
    })

    const newRMapping = new Mapping([map.invert()])
    newRMapping.appendMapping(rMapping)
    rMapping = newRMapping

    return acc
  }, remappedOldPoints)

  // remove replacement points which contain redundant information
  const prunedReplacements = rawSpans
    // sort by length
    .sort(
      (a, b) => b.ancestorTo - b.ancestorFrom - (a.ancestorTo - a.ancestorFrom)
    )
    // exclude any span which is contained by another
    .reduce((acc, point) => {
      if (
        acc.find(
          (existing) =>
            existing.ancestorFrom <= point.ancestorFrom &&
            existing.ancestorTo >= point.ancestorTo
        )
      ) {
        return acc
      }
      return acc.concat(point)
    }, [] as Replacements)

  return {
    replacements: prunedReplacements,
    forwardMapping: fMapping,
    reverseMapping: rMapping,
  }
}

export const updateDiff = (diff: Diff, tr: Transform): Diff => {
  return updateDiffInner(diff, tr.steps, tr.mapping)
}

export const createDiff = (commit: Commit): Diff => {
  const steps = smoosh<Step>(commit, (c) => c.steps)

  return updateDiffInner(
    {
      forwardMapping: new Mapping(),
      reverseMapping: new Mapping(),
      replacements: [],
    },
    steps,
    new Mapping(steps.map((s) => s.getMap()))
  )
}
