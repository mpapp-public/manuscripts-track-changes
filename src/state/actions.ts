/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Transaction } from 'prosemirror-state'

import { trackPluginKey, TrackPluginState } from '../plugin'
import {
  addAnnotation,
  loadAnnotations,
  removeAnnotation,
  updateAnnotations,
} from './annotations'
import { freeze, updateCommit } from './commit'
import { createDiff, updateDiff } from './diff'

export enum TRACK_PLUGIN_ACTIONS {
  FREEZE = 'FREEZE',
  REPLACE = 'REPLACE',
  ADD_ANNOTATION = 'ADD_ANNOTATION',
  REMOVE_ANNOTATION = 'REMOVE_ANNOTATION',
}
const a = TRACK_PLUGIN_ACTIONS

const applyTransform = (
  state: TrackPluginState,
  tr: Transaction
): TrackPluginState => {
  if (!tr.docChanged) {
    return state
  }

  return {
    ...state,
    commit: updateCommit(state.commit, tr),
    diff: updateDiff(state.diff, tr),
    annotations: updateAnnotations(state.annotations, tr),
  }
}

export default (state: TrackPluginState, tr: Transaction): TrackPluginState => {
  const action = tr.getMeta(trackPluginKey)

  const preparedState = applyTransform(state, tr)

  if (!action) {
    return preparedState
  }

  switch (action.type) {
    case a.FREEZE: {
      return {
        ...preparedState,
        commit: freeze(preparedState.commit),
      }
    }
    case a.REPLACE: {
      const diff = createDiff(action.commit)
      return {
        ...preparedState,
        diff,
        commit: action.commit,
        annotations: loadAnnotations(action.annotations, diff.forwardMapping),
      }
    }
    case a.ADD_ANNOTATION: {
      return {
        ...preparedState,
        annotations: addAnnotation(
          preparedState.annotations,
          action.annotation,
          preparedState.diff.reverseMapping
        ),
      }
    }
    case a.REMOVE_ANNOTATION: {
      return {
        ...preparedState,
        annotations: removeAnnotation(preparedState.annotations, action.uid),
      }
    }
    default: {
      return preparedState
    }
  }
}
