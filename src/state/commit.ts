/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Step, Transform } from 'prosemirror-transform'
import { v4 as uuid } from 'uuid'

import { getCommitID } from '../hash'
import { Span, updateBlame } from './blame'

export interface Commit {
  _id: string
  changeID: string
  blame: Span[]
  steps: Step[]
  prev: Commit | null
  createdAt: number
  updatedAt?: number
}

export const smoosh = <T>(
  commit: Commit,
  selector: (commit: Commit) => T | Array<T>
): Array<T> => {
  const getFromSelector = () => {
    const result = selector(commit)
    return Array.isArray(result) ? result : [result]
  }
  if (commit.prev) {
    return smoosh(commit.prev, selector).concat(getFromSelector())
  }
  return getFromSelector()
}

export const buildCommit = (
  data: Omit<Commit, '_id' | 'updatedAt' | 'createdAt'>
): Commit => {
  const changeIDs = data.prev
    ? [data.changeID, ...smoosh(data.prev, (c) => c.changeID)]
    : [data.changeID]

  return {
    _id: getCommitID(changeIDs),
    updatedAt: Date.now() / 1000,
    createdAt: Date.now() / 1000,
    ...data,
  }
}

export const emptyCommit = (): Commit => {
  return buildCommit({
    changeID: uuid(),
    blame: [],
    steps: [],
    prev: null,
  })
}

export const freeze = (prev: Commit, tr?: Transform): Commit => {
  const changeID = uuid()

  return buildCommit({
    changeID,
    blame: tr ? updateBlame(prev.blame, tr.mapping, changeID) : prev.blame,
    steps: tr ? tr.steps : [],
    prev,
  })
}

export const updateCommit = (commit: Commit, tr: Transform) => {
  const newBlame = updateBlame(commit.blame, tr.mapping, commit.changeID)

  return {
    ...commit,
    updatedAt: Date.now() / 1000,
    blame: newBlame,
    steps: commit.steps.concat(tr.steps),
  }
}
