/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Step } from 'prosemirror-transform'

import {
  Commit,
  emptyCommit,
  freeze,
  smoosh,
  updateCommit,
} from '../state/commit'
import { initialState, typeSomething } from './helpers'

describe('updateCommit', () => {
  it('produces a blame map tracking any new additions', () => {
    const state = initialState()
    const commit = emptyCommit()

    const { tr } = typeSomething(state, [[53, 'i']])
    const nextCommit = updateCommit(commit, tr)

    expect(nextCommit.steps).toHaveLength(1)
    expect(nextCommit.blame).toHaveLength(1)
  })

  it('groups contiguous spans in the blame map', () => {
    const state = initialState()
    const commit = emptyCommit()

    const { tr } = typeSomething(state, [
      [53, 'A'],
      [54, 'B'],
    ])
    const nextCommit = updateCommit(commit, tr)

    expect(nextCommit.steps).toHaveLength(2)
    expect(nextCommit.blame).toHaveLength(1)
  })

  it('creates new spans for discontiguous changes', () => {
    const state = initialState()
    let commit = emptyCommit()

    const { tr } = typeSomething(state, [[53, 'A']])
    commit = updateCommit(commit, tr)

    const { tr: tr2 } = typeSomething(state, [[60, 'B']])
    commit = updateCommit(commit, tr2)

    expect(commit.steps).toHaveLength(2)
    expect(commit.blame).toHaveLength(2)
  })
})

describe('freeze', () => {
  it('should place the commit inside a previous commit', () => {
    const state = initialState()
    const commit = emptyCommit()

    const { tr } = typeSomething(state, [
      [53, 'A'],
      [54, 'B'],
    ])
    const next = updateCommit(commit, tr)
    const frozen = freeze(next)

    expect(frozen._id).not.toEqual(next._id)
    expect(frozen.blame).toEqual(next.blame)
    expect(frozen.prev).toEqual(next)
  })
})

describe('smoosh', () => {
  it('should provide a way to get properties of each commit as a list from inner to outer', () => {
    const state = initialState()
    const commit = emptyCommit()

    const { tr } = typeSomething(state, [
      [53, 'A'],
      [54, 'B'],
    ])
    const next = updateCommit(commit, tr)
    const nested = freeze(next)

    const selector = (commit: Commit) => commit._id

    const result = smoosh<string>(nested, selector)
    expect(result).toHaveLength(2)
    expect(result[0]).toEqual(next._id)
  })

  it('should create a flat array from properties that are an array', () => {
    const state = initialState()
    const commit = emptyCommit()

    const { tr } = typeSomething(state, [
      [53, 'A'],
      [54, 'B'],
    ])
    const next = updateCommit(commit, tr)
    const nested = freeze(next)

    const selector = (commit: Commit) => commit.steps

    const result = smoosh<Step>(nested, selector)
    expect(result).toHaveLength(2)
    expect(result[0]).toBeInstanceOf(Step)
    expect(result[0]).toEqual(next.steps[0])
  })
})
