/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { schema } from 'prosemirror-schema-basic'
import { Step } from 'prosemirror-transform'

import * as lib from '../lib'
import {
  Commit,
  emptyCommit,
  freeze,
  smoosh,
  updateCommit,
} from '../state/commit'
import { applyTr, initialState, typeSomething } from './helpers'

test('stringify a commit and get it back', () => {
  let state = initialState()
  let commit = emptyCommit()

  // set up a commit with steps and with a step in the prev
  const { tr, state: _state } = typeSomething(state, [[60, 'A']])
  state = _state
  commit = updateCommit(commit, tr)
  commit = freeze(commit)

  const { tr: tr2, state: __state } = typeSomething(state, [[53, 'B']])
  state = __state
  commit = updateCommit(commit, tr2)

  const json = lib.commitToJSON(commit, 'containerID')

  const reconstructedCommit: Commit = lib.commitFromJSON(json, schema)

  const steps = smoosh<Step>(commit, (c) => c.steps)
  const reconstructedSteps = smoosh<Step>(reconstructedCommit, (c) => c.steps)

  const nextState = applyTr(initialState(), steps)
  const reconstructedState = applyTr(initialState(), reconstructedSteps)

  expect(nextState).toEqual(reconstructedState)
})

describe('rebases', () => {
  const { without: rebaseWithout, cherryPick } = lib.rebases

  describe('without', () => {
    it('should replay all the steps', () => {
      const state = initialState()
      const commit = emptyCommit()

      const { tr } = typeSomething(state, [
        [53, 'B'],
        [60, 'A'],
      ])
      const next = updateCommit(commit, tr)
      const { commit: remapped } = rebaseWithout(next, [])

      if (!remapped) {
        throw new Error('Unexpected null from rebaseWithout')
      }

      expect(next.changeID).toEqual(remapped.changeID)
      expect(next.blame).toEqual(remapped.blame)
      expect(next._id).toEqual(remapped._id)

      const nextState = applyTr(initialState(), next.steps)
      const remappedState = applyTr(initialState(), remapped.steps)
      expect(nextState).toEqual(remappedState)
    })

    it('should remap all the steps', () => {
      const state = initialState()
      const commit = emptyCommit()

      const { tr } = typeSomething(state, [
        [60, 'A'],
        [53, 'B'],
      ])
      const next = updateCommit(commit, tr)
      const { commit: remapped } = rebaseWithout(next, [])

      if (!remapped) {
        throw new Error('Unexpected null from rebaseWithout')
      }

      expect(next.changeID).toEqual(remapped.changeID)
      expect(next.blame).toEqual(remapped.blame)
      expect(next._id).toEqual(remapped._id)

      const nextState = applyTr(initialState(), next.steps)
      const remappedState = applyTr(initialState(), remapped.steps)
      expect(nextState).toEqual(remappedState)
    })

    it('should remap deletions', () => {
      const state = initialState()
      const commit = emptyCommit()

      const { tr } = state
      // delete some text
      tr.replace(53, 60)
      const next = updateCommit(commit, tr)
      const { commit: remapped } = rebaseWithout(next, [])

      if (!remapped) {
        throw new Error('Unexpected null from rebaseWithout')
      }

      expect(next.changeID).toEqual(remapped.changeID)
      expect(next.blame).toEqual(remapped.blame)
      expect(next._id).toEqual(remapped._id)

      const nextState = applyTr(initialState(), next.steps)
      const remappedState = applyTr(initialState(), remapped.steps)
      expect(nextState).toEqual(remappedState)
    })

    it('should remap across multiple commits', () => {
      let state = initialState()
      let commit = emptyCommit()

      // set up a commit with steps and with a step in the prev
      const { tr, state: _state } = typeSomething(state, [[60, 'A']])
      state = _state
      commit = updateCommit(commit, tr)
      commit = freeze(commit)

      const { tr: tr2, state: __state } = typeSomething(state, [[53, 'B']])
      state = __state
      commit = updateCommit(commit, tr2)

      const { commit: remapped } = rebaseWithout(commit, [])

      if (!remapped) {
        throw new Error('Unexpected null from rebaseWithout')
      }

      // the blames should match
      expect(commit.changeID).toEqual(remapped.changeID)
      expect(commit.blame).toEqual(remapped.blame)
      expect(commit._id).toEqual(remapped._id)

      const steps = smoosh<Step>(commit, (c) => c.steps)
      const remappedSteps = smoosh<Step>(remapped, (c) => c.steps)

      // the two states derived from the commit:
      const nextState = applyTr(initialState(), steps)
      const remappedState = applyTr(initialState(), remappedSteps)
      expect(nextState).toEqual(remappedState)
    })

    it('should allow you to exclude commits', () => {
      let state = initialState()
      let commit = emptyCommit()

      // set up a commit with steps and with a step in the prev
      const { tr, state: _state } = typeSomething(state, [[60, 'A']])
      state = _state
      commit = updateCommit(commit, tr)
      commit = freeze(commit)

      const { tr: tr2, state: __state } = typeSomething(state, [[53, 'B']])
      state = __state
      commit = updateCommit(commit, tr2)

      const { commit: remapped } = rebaseWithout(commit, [
        commit.prev!.changeID,
      ])

      if (!remapped) {
        throw new Error('Unexpected null from rebaseWithout')
      }

      expect(commit.blame).toHaveLength(2)
      expect(remapped.blame).toHaveLength(1)
      expect(commit.blame[0].from).toEqual(remapped.blame[0].from)
      expect(commit.blame[0].to).toEqual(remapped.blame[0].to)
    })
  })

  describe('cherryPick', () => {
    it('should put commits together that are not based on each other', () => {
      const state = initialState()
      let commit = emptyCommit()

      // set up a commit with steps and with a step in the prev
      const { tr } = typeSomething(state, [[60, 'A']])
      commit = updateCommit(commit, tr)

      const { tr: tr2 } = typeSomething(state, [[53, 'B']])
      const unrelatedCommit = updateCommit(commit, tr2)

      const { commit: remapped } = cherryPick(commit, unrelatedCommit)
      const { commit: remappedReverse } = cherryPick(unrelatedCommit, commit)

      if (!remapped) {
        throw new Error('Unexpected null from cherryPick')
      }
      if (!remappedReverse) {
        throw new Error('Unexpected null from cherryPick')
      }

      // the blames should match
      expect(remapped.blame).toEqual(remappedReverse.blame)
      expect(remapped._id).toEqual(remappedReverse._id)

      const steps = smoosh<Step>(remapped, (c) => c.steps)
      const stepsReversed = smoosh<Step>(remappedReverse, (c) => c.steps)

      // the two states derived from the commit:
      const nextState = applyTr(initialState(), steps)
      const reversedState = applyTr(initialState(), stepsReversed)
      expect(nextState).toEqual(reversedState)
    })
  })
})

describe('stitch', () => {
  it('should put commits together that are not based on each other', () => {
    const state = initialState()

    // set up a commit with steps and with a step in the prev
    const { tr } = typeSomething(state, [[60, 'A']])
    const commit = updateCommit(emptyCommit(), tr)

    const { tr: tr2 } = typeSomething(state, [[53, 'B']])
    const unrelatedCommit = updateCommit(emptyCommit(), tr2)

    const remapped = lib.synthetizeCommit(
      [commit, unrelatedCommit],
      [commit.changeID, unrelatedCommit.changeID]
    )
    const remappedReverse = lib.synthetizeCommit(
      [unrelatedCommit, commit],
      [unrelatedCommit.changeID, commit.changeID]
    )

    if (!remapped) {
      throw new Error('Unexpected null from cherryPick')
    }
    if (!remappedReverse) {
      throw new Error('Unexpected null from cherryPick')
    }

    // the blames should match
    expect(remapped.blame).toEqual(remappedReverse.blame)

    const steps = smoosh<Step>(remapped, (c) => c.steps)
    const stepsReversed = smoosh<Step>(remappedReverse, (c) => c.steps)

    // the two states derived from the commit:
    const nextState = applyTr(initialState(), steps)
    const reversedState = applyTr(initialState(), stepsReversed)
    expect(nextState).toEqual(reversedState)
  })
})
