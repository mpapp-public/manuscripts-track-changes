/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { emptyCommit, updateCommit } from '../state/commit'
import { createDiff, updateDiff } from '../state/diff'
import { deleteSomething, initialState, typeSomething } from './helpers'

describe('createDiff', () => {
  it('tracks the positions deleted in the original document', () => {
    const state1 = initialState()
    const commit1 = emptyCommit()

    const { tr } = deleteSomething(state1, [5, 7])
    const commit2 = updateCommit(commit1, tr)
    const { replacements } = createDiff(commit2)

    expect(replacements).toHaveLength(1)
    expect(replacements[0]).toEqual({ ancestorFrom: 5, ancestorTo: 7, pos: 5 })
  })
})

describe('updateDiff', () => {
  it('shifts the positions of the existing diff without changing their positions in the original doc', () => {
    const state0 = initialState()
    const commit0 = emptyCommit()

    const { tr, state: state1 } = deleteSomething(state0, [5, 7])
    const commit1 = updateCommit(commit0, tr)
    const diff1 = createDiff(commit1)

    const { tr: tr2 } = typeSomething(state1, [[0, 'Hi!']])
    const { replacements } = updateDiff(diff1, tr2)

    expect(replacements).toHaveLength(1)
    expect(replacements[0]).toEqual({ ancestorFrom: 5, ancestorTo: 7, pos: 10 })
  })

  it('adds further deletions and remaps the existing ones', () => {
    const state0 = initialState()
    const commit0 = emptyCommit()

    const { tr, state: state1 } = deleteSomething(state0, [5, 7])
    const commit1 = updateCommit(commit0, tr)
    const diff1 = createDiff(commit1)

    const { tr: tr2 } = deleteSomething(state1, [0, 2])
    const { replacements } = updateDiff(diff1, tr2)

    expect(replacements).toHaveLength(2)
    expect(replacements[0]).toEqual({ ancestorFrom: 5, ancestorTo: 7, pos: 4 })
    expect(replacements[1]).toEqual({ ancestorFrom: 0, ancestorTo: 2, pos: 0 })
  })
})
