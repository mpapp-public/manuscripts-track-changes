/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const hashString = (input: string) => {
  let hash = 0

  if (input.length === 0) {
    return btoa('0')
  }

  for (let i = 0; i < input.length; i++) {
    const charCode = input.charCodeAt(i)
    hash = (hash << 7) - hash + charCode
    hash = hash & hash
  }
  return btoa(hash.toString())
}

export const getCommitID = (changeIDs: string[]): string => {
  return `MPCommit:${hashString(changeIDs.slice().sort().join())}`
}
