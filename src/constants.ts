/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const BASE_CLASS = 'track-changes'

export enum CLASSES {
  blame = 'blame',
  blameUncommitted = 'blame-uncommitted',
  blamePoint = 'blame-point',
  diffReplaced = 'replaced',
  annotation = 'annotation',
  focused = 'focused',
  control = 'control',
  annotationControl = 'annotation-control',
}

export const ANNOTATION_COLOR_VAR = 'annotation-color'

export const ACCEPT_BUTTON_XLINK = '#track-changes-action-accept'

export const REJECT_BUTTON_XLINK = '#track-changes-action-reject'

export const ANN_CONTROL_XLINK = '#track-changes-ann-select-comment'
