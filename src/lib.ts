/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Commit as CommitJson,
  ObjectTypes,
} from '@manuscripts/manuscripts-json-schema'
import { Schema } from 'prosemirror-model'
import { EditorState, Selection, TextSelection } from 'prosemirror-state'
import { Mapping, Step } from 'prosemirror-transform'

import { getCommitID } from './hash'
import { getTrackPluginState } from './plugin'
import { updateBlame } from './state/blame'
import { buildCommit, Commit, emptyCommit, smoosh } from './state/commit'

export interface ChangeSummary {
  insertion: string
  deletion: string
  ancestorPos: number
}

export interface GenericSpan {
  from: number
  to: number
}

export const getCommitsList = (state: EditorState) => {
  const { commit } = getTrackPluginState(state)
  return smoosh(commit, (c) => ({ _id: c._id, changeID: c.changeID }))
}

export const isTextSelection = (
  selection: Selection
): selection is TextSelection => selection instanceof TextSelection

export const commitToJSON = (
  commit: Commit,
  containerID: string
): CommitJson => {
  const recur = (commit: Commit): CommitJson => ({
    updatedAt: Date.now() / 1000,
    ...commit,
    steps: commit.steps.map((step) => step.toJSON()),
    prev: commit.prev ? recur(commit.prev) : undefined,
    containerID,
    objectType: ObjectTypes.Commit,
  })

  return recur(commit)
}

export const commitFromJSON = (json: CommitJson, schema: Schema): Commit => {
  const recur = (json: CommitJson): Commit => ({
    _id: json._id,
    changeID: json.changeID,
    blame: json.blame,
    prev: json.prev ? recur(json.prev) : null,
    steps: json.steps.map((step) => Step.fromJSON(schema, step)),
    createdAt: json.createdAt,
    updatedAt: json.updatedAt,
  })

  return recur(json)
}

export const findCommitWithin = (commit: Commit) => (
  id: string
): Commit | null => {
  if (commit._id === id || commit.changeID === id) {
    return commit
  }

  if (!commit.prev) {
    return null
  }

  return findCommitWithin(commit.prev)(id)
}

export const getChangeSummary = (
  state: EditorState,
  /**
   * Default: The summary for the current commit
   */
  changeID?: string
): ChangeSummary | null => {
  const pluginState = getTrackPluginState(state)
  const commit = changeID
    ? findCommitWithin(pluginState.commit)(changeID)
    : pluginState.commit

  if (!commit) {
    return null
  }

  // find the position of the first span and map it to the ancestor document
  const spans = commit.blame.filter((span) => span.commit === commit.changeID)
  if (!spans.length) {
    return null
  }
  const { from: pos } = spans[0]
  const ancestorPos = pluginState.diff.reverseMapping.map(pos, -1)

  // find the text within the first span that contains content
  const firstSpanWithContent = spans.find((span) => span.to > span.from)
  const insertion = firstSpanWithContent
    ? state.doc.textBetween(firstSpanWithContent.from, firstSpanWithContent.to)
    : ''

  if (!pluginState.config.ancestorDoc) {
    return {
      insertion,
      ancestorPos,
      deletion: '',
    }
  }

  // find the replacement span that is adjacent to one of the insertion spans
  const replacement = pluginState.diff.replacements.find((replacement) => {
    return spans.find(
      (span) =>
        typeof replacement.pos !== 'undefined' &&
        replacement.pos >= span.from &&
        replacement.pos <= span.to
    )
  })
  const deletion = replacement
    ? pluginState.config.ancestorDoc.textBetween(
        replacement.ancestorFrom,
        replacement.ancestorTo
      )
    : ''

  return { insertion, deletion, ancestorPos }
}

export const doSpansOverlap = <A extends GenericSpan, B extends GenericSpan>(
  a: A,
  b: B
) => {
  return !(a.from > b.to || a.to < b.from)
}

export const isCommitContiguousWithSelection = (
  state: EditorState,
  /**
   * Default: the current commit
   */
  changeID?: string
): boolean => {
  const pluginState = getTrackPluginState(state)
  const { selection } = state
  const commit = changeID
    ? findCommitWithin(pluginState.commit)(changeID)
    : pluginState.commit

  if (!commit) {
    return false
  }

  return !!pluginState.commit.blame.find(
    (span) => span.commit === commit.changeID && doSpansOverlap(span, selection)
  )
}

export const focusedEntities = (state: EditorState) => {
  const { selection } = state
  if (!isTextSelection(selection)) {
    return { commit: null, annotation: null }
  }

  const pluginState = getTrackPluginState(state)
  const { blame } = pluginState.commit
  const commit =
    blame.find((span) => doSpansOverlap(span, selection))?.commit || null

  const { annotations } = pluginState
  const annotation =
    annotations.find((ann) => doSpansOverlap(ann, selection))?.uid || null

  return { commit, annotation }
}

const hashCommit = (commit: Commit): string => {
  return getCommitID(smoosh(commit, (c) => c.changeID))
}

export const findCommitWithChanges = (
  commits: Commit[],
  changeIDs: string[]
) => {
  const checksum = getCommitID(changeIDs)
  return commits.find((commit) => hashCommit(commit) === checksum)
}

export const reverseMapping = (steps: Step[]): Mapping =>
  new Mapping(
    steps
      .slice()
      .reverse()
      .map((step) => step.getMap().invert())
  )

// NOTE: innerMapping is the mapping between the start of "pick"
// as it currently sits to its new position.
const rebase = (pick: Commit, onto: Commit | null, innerMapping: Mapping) => {
  const mapping = reverseMapping(pick.steps)
  mapping.appendMapping(innerMapping)

  // Remap each step by taking it all the way back through all the reverse steps
  // so far, and then forward through to its current state.
  const remappedSteps = pick.steps
    .map((step, i) => {
      const rI = pick.steps.length - 1 - i

      // slice off the reverse steps after this one
      const remapped = step.map(mapping.slice(rI + 1))
      if (!remapped) {
        return null
      }

      mapping.appendMap(remapped.getMap(), rI)
      return remapped
    })
    .filter(Boolean) as Step[]

  const blame = updateBlame(
    // start from the previous commits blame (or an empty blame)
    onto ? onto.blame : [],
    // update it with the mapping corresponding to ONLY this commit's steps
    new Mapping(remappedSteps.map((step) => step.getMap())),
    pick.changeID
  )

  return {
    commit: buildCommit({
      changeID: pick.changeID,
      steps: remappedSteps,
      blame,
      prev: onto,
    }),
    mapping,
  }
}

const rebaseWithout = (
  commit: Commit,
  without: string[]
): { commit: Commit | null; mapping: Mapping } => {
  // we start by remapping the prev, if there is one.
  // If there is no prev commit, begin by setting to null and generating
  // an empty mapping
  const { commit: prev, mapping } = commit.prev
    ? rebaseWithout(commit.prev, without)
    : { commit: null, mapping: new Mapping() }

  // if the commit is set to be exluded, we can hoist the child commit. BUT
  // we must add this commit's steps in reverse to the mapping so that the commits
  // above this one can be rebased.
  if (without.includes(commit.changeID)) {
    const mappingWithReverseSteps = reverseMapping(commit.steps)
    mappingWithReverseSteps.appendMapping(mapping)
    return { commit: prev, mapping: mappingWithReverseSteps }
  }

  // Remap each step by taking it all the way back through all the reverse steps
  // so far, and then forward through to its current state.
  return rebase(commit, prev, mapping)
}

// TODO: Compose with optimizers:
// If pick and onto share a common ancestor, both mappings can
// be stopped at that point.
// Do not remap empty commits
const cherryPick = (pick: Commit, onto: Commit) => {
  const mapping = pick.prev
    ? reverseMapping(smoosh<Step>(onto, (c) => c.steps))
    : new Mapping()

  const fMap = new Mapping(
    smoosh<Step>(onto, (c) => c.steps).map((s) => s.getMap())
  )
  mapping.appendMapping(fMap)

  return rebase(pick, onto, mapping)
}

export const synthetizeCommit = (
  commits: Commit[],
  changeIDs: string[]
): Commit => {
  return changeIDs.reduce((acc, changeID) => {
    if (findCommitWithin(acc)(changeID)) {
      return acc
    }
    const pick = commits.find((c) => c.changeID === changeID)
    if (!pick) {
      return acc
    }

    return cherryPick(pick, acc).commit
  }, emptyCommit())
}

export const rebases = {
  without: rebaseWithout,
  cherryPick,
}
