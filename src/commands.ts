/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Command } from 'prosemirror-commands'
import { TextSelection } from 'prosemirror-state'

import { isTextSelection } from './lib'
import { getTrackPluginState, trackPluginKey } from './plugin'
import { TRACK_PLUGIN_ACTIONS as $ } from './state/actions'

interface Coords {
  from: number
  to: number
}

const focusCommit = (changeID: string): Command => (state, dispatch, view) => {
  const { tr } = state
  const pluginState = getTrackPluginState(state)

  // make sure there is at least one span corresponding to that commit
  // in the current blame.
  const span = pluginState.commit.blame.find((span) => span.commit === changeID)
  if (!span) {
    return false
  }
  if (!dispatch || !view) {
    return true
  }

  tr.setMeta('addToHistory', false)
  view.focus()
  tr.setSelection(TextSelection.create(state.doc, span.to)).scrollIntoView()
  dispatch(tr)
  return true
}

const focusAnnotation = (uid: string): Command => (state, dispatch, view) => {
  const { tr } = state
  const pluginState = getTrackPluginState(state)

  // make sure there is at least one span corresponding to that commit
  // in the current blame.
  const span = pluginState.annotations.find((span) => span.uid === uid)
  if (!span) {
    return false
  }
  if (!dispatch || !view) {
    return true
  }

  tr.setMeta('addToHistory', false)

  // Instead of using just scrollIntoView to scroll into the annotation, we must include how much the
  // editor has been scrolled from the top (view.dom.scrollTop). Otherwise the editor will scroll the
  // element just to the edge of the viewport, sometimes invisible (as it gets hidden by the toolbar)
  // and in general is not very great user experience.
  const elPos = view.coordsAtPos(span.from)
  view.dom.scrollTo(0, elPos.top + view.dom.scrollTop)

  view.focus()
  tr.setSelection(TextSelection.create(state.doc, span.from))
  tr.scrollIntoView()

  dispatch(tr)
  return true
}

const freezeCommit = (): Command => (state, dispatch) => {
  const { commit } = getTrackPluginState(state)
  if (!commit.steps.length) {
    return false
  }

  if (dispatch) {
    state.tr.setMeta('addToHistory', false)
    dispatch(state.tr.setMeta(trackPluginKey, { type: $.FREEZE }))
  }

  return true
}

const addAnnotation = (
  uid: string,
  color: string,
  coords?: Coords
): Command => (state, dispatch) => {
  let from: number
  let to: number
  if (coords) {
    from = coords.from
    to = coords.to
  } else if (isTextSelection(state.selection)) {
    from = state.selection.from
    to = state.selection.to
  } else {
    return false
  }

  const updatedAt = Date.now() / 1000

  if (dispatch) {
    state.tr.setMeta('addToHistory', false)
    dispatch(
      state.tr.setMeta(trackPluginKey, {
        type: $.ADD_ANNOTATION,
        annotation: {
          from,
          to,
          uid,
          color,
          updatedAt,
        },
      })
    )
  }

  return true
}

const removeAnnotation = (uid: string): Command => (state, dispatch) => {
  if (dispatch) {
    state.tr.setMeta('addToHistory', false)
    dispatch(
      state.tr.setMeta(trackPluginKey, {
        type: $.REMOVE_ANNOTATION,
        uid,
      })
    )
  }
  return true
}

export const commands = {
  focusCommit,
  focusAnnotation,
  freezeCommit,
  addAnnotation,
  removeAnnotation,
}
