/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { EditorState } from 'prosemirror-state'
import { Decoration, DecorationSet } from 'prosemirror-view'

import { Annotation } from '.'
import {
  ACCEPT_BUTTON_XLINK,
  ANN_CONTROL_XLINK,
  ANNOTATION_COLOR_VAR,
  BASE_CLASS,
  CLASSES,
  REJECT_BUTTON_XLINK,
} from './constants'
import { doSpansOverlap } from './lib'
import { getTrackPluginState } from './plugin'
import { Span } from './state/blame'

export const getClassnames = (...types: Array<CLASSES | null>): string[] => {
  return [BASE_CLASS].concat(
    types.filter(Boolean).map((type) => `${BASE_CLASS}--${type}`)
  )
}

export const addClassnamesToEl = (
  el: HTMLElement,
  ...types: Array<CLASSES | null>
) => {
  getClassnames(...types).forEach((classname) => el.classList.add(classname))
}

const decorateDiff = (state: EditorState) => {
  const pluginState = getTrackPluginState(state)
  const { ancestorDoc } = pluginState.config

  if (!ancestorDoc) {
    return []
  }

  return pluginState.diff.replacements
    .map((span) => {
      if (!span.pos) {
        return null
      }
      const blameSpan = pluginState.commit.blame.find(
        (blameSpan) => blameSpan.from === span.pos || blameSpan.to === span.pos
      )
      const isFocused = blameSpan && doSpansOverlap(blameSpan, state.selection)

      return Decoration.widget(span.pos, () => {
        const el = document.createElement('span')

        addClassnamesToEl(
          el,
          CLASSES.diffReplaced,
          isFocused ? CLASSES.focused : null
        )

        el.textContent = ancestorDoc.textBetween(
          span.ancestorFrom,
          span.ancestorTo
        )

        if (blameSpan) {
          el.dataset.changeid = blameSpan.commit
        }
        return el
      })
    })
    .filter(Boolean) as Decoration[]
}

const blamePointClasses = getClassnames(CLASSES.blamePoint)

const createBlameDecoration = (
  spans: Span[],
  isUncommitted: boolean,
  isFocused: boolean
) => {
  return spans.map((span) => {
    const { from, to, commit } = span
    if (from === to) {
      return Decoration.widget(
        from,
        () => {
          const el = document.createElement('span')
          blamePointClasses.forEach((classname) => {
            el.classList.add(classname)
          })
          el.dataset.changeid = span.commit
          return el
        },
        { side: -1 }
      )
    }

    const classNames = getClassnames(
      CLASSES.blame,
      isUncommitted ? CLASSES.blameUncommitted : null,
      isFocused ? CLASSES.focused : null
    )

    return Decoration.inline(
      from,
      to,
      {
        class: classNames.join(' '),
        ['data-changeid']: commit,
      },
      {
        inclusiveEnd: true,
      }
    )
  })
}

const createBlameControl = (span: Span, action: string, xLink: string) => {
  return `
    <button type="button" data-action="${action}" data-changeid="${span.commit}">
      <svg><use xlink:href="${xLink}"></use></svg>
    </button>
  `
}

const createControls = (spans: Span[], state: EditorState) => {
  return spans.map((span) => {
    return Decoration.widget(span.to, () => {
      const isFocused = doSpansOverlap(span, state.selection)
      const el = document.createElement('div')
      addClassnamesToEl(el, CLASSES.control, isFocused ? CLASSES.focused : null)
      el.dataset.changeid = span.commit
      el.innerHTML =
        createBlameControl(span, 'reject', REJECT_BUTTON_XLINK) +
        createBlameControl(span, 'accept', ACCEPT_BUTTON_XLINK)
      return el
    })
  })
}

const splitSpanByBlocks = (span: Span, state: EditorState): Span[] => {
  const ends: number[] = [span.to]
  state.doc.nodesBetween(span.from, span.to, (node, pos) => {
    if (node.type.isBlock) {
      const end = pos + node.nodeSize - 1
      if (end < span.to) {
        ends.push(end)
      }
    }
  })
  return ends.sort().map((end, i) =>
    i === 0
      ? {
          from: span.from,
          to: end,
          commit: span.commit,
        }
      : {
          from: ends[i - 1],
          to: end,
          commit: span.commit,
        }
  )
}

const decorateBlame = (
  state: EditorState,
  showBlameSpanButtons?: boolean
): Decoration[] => {
  const pluginState = getTrackPluginState(state)
  const { blame } = pluginState.commit

  return blame.reduce((decorations, span) => {
    if (span.commit === null) {
      return decorations
    }
    const isUncommitted = span.commit === pluginState.commit.changeID
    const isFocused = doSpansOverlap(span, state.selection)
    if (!showBlameSpanButtons || isUncommitted) {
      return [
        ...decorations,
        ...createBlameDecoration([span], isUncommitted, isFocused),
      ]
    }

    const subspans = splitSpanByBlocks(span, state)

    return [
      ...decorations,
      ...createBlameDecoration(subspans, isUncommitted, isFocused),
      ...createControls(subspans, state),
    ]
  }, [] as Decoration[])
}

export const decorateAnnotationSpans = (state: EditorState) => {
  const pluginState = getTrackPluginState(state)
  return pluginState.annotations.map((ann) => {
    const isFocused = doSpansOverlap(ann, state.selection)
    const classes = getClassnames(
      CLASSES.annotation,
      isFocused ? CLASSES.focused : null
    )

    return Decoration.inline(ann.from, ann.to, {
      class: classes.join(' '),
      style: `--${ANNOTATION_COLOR_VAR}: ${ann.color}`,
      ['data-uid']: ann.uid,
    })
  })
}

const createAnnotationControl = (ann: Annotation) => {
  return `
  <button type="button" data-action="select-comment" data-uid="${ann.uid}">
    <svg><use xlink:href="${ANN_CONTROL_XLINK}"></use></svg>
  </button>
  `
}

export const decorateAnnoationControls = (state: EditorState) => {
  const pluginState = getTrackPluginState(state)
  return pluginState.annotations.map((ann) => {
    return Decoration.widget(ann.from, () => {
      const el = document.createElement('div')
      const isFocused = doSpansOverlap(ann, state.selection)
      addClassnamesToEl(
        el,
        CLASSES.annotationControl,
        isFocused ? CLASSES.focused : null
      )
      el.innerHTML = createAnnotationControl(ann)

      el.setAttribute('style', `--${ANNOTATION_COLOR_VAR}: ${ann.color}`)
      return el
    })
  })
}

export const createDecorations = (
  state: EditorState,
  showBlameSpanButtons?: boolean
) => {
  return DecorationSet.create(state.doc, [
    ...decorateDiff(state),
    ...decorateBlame(state, showBlameSpanButtons),
    ...decorateAnnotationSpans(state),
    ...decorateAnnoationControls(state),
  ])
}
