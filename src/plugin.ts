/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Node as ProsemirrorNode } from 'prosemirror-model'
import { EditorState, Plugin, PluginKey } from 'prosemirror-state'

import { createDecorations } from './dom'
import applyAction from './state/actions'
import { Annotation, loadAnnotations } from './state/annotations'
import { Commit, emptyCommit } from './state/commit'
import { createDiff, Diff, emptyDiff } from './state/diff'

export interface TrackPluginState {
  config: {
    ancestorDoc: ProsemirrorNode | null
  }
  commit: Commit
  diff: Diff
  annotations: Annotation[]
}

interface TrackPluginConfig {
  commit?: Commit
  ancestorDoc?: ProsemirrorNode
  annotations?: Array<Omit<Annotation, 'from' | 'to'>>
  showBlameSpanButtons?: boolean
}

export const trackPluginKey = new PluginKey('track-changes-plugin')

export default ({
  commit,
  ancestorDoc,
  annotations,
  showBlameSpanButtons,
}: TrackPluginConfig = {}) => {
  const trackPlugin: Plugin<TrackPluginState> = new Plugin({
    key: trackPluginKey,

    state: {
      init(): TrackPluginState {
        const diff = commit ? createDiff(commit) : emptyDiff()
        return {
          config: {
            ancestorDoc: ancestorDoc || null,
          },
          commit: commit || emptyCommit(),
          diff,
          annotations: annotations
            ? loadAnnotations(annotations, diff.forwardMapping)
            : [],
        }
      },

      apply(tr, state: TrackPluginState) {
        return applyAction(state, tr)
      },
    },
    props: {
      decorations(state) {
        return createDecorations(state, showBlameSpanButtons)
      },
    },
  })

  return trackPlugin
}

export const getTrackPluginState = (state: EditorState) =>
  trackPluginKey.getState(state) as TrackPluginState
