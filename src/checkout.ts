/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Node as ProsemirrorNode } from 'prosemirror-model'
import { EditorState } from 'prosemirror-state'
import { Mapping, Step } from 'prosemirror-transform'

import { reverseMapping } from './lib'
import { getTrackPluginState, trackPluginKey } from './plugin'
import { TRACK_PLUGIN_ACTIONS } from './state/actions'
import { Commit, emptyCommit, freeze, smoosh } from './state/commit'

export const checkout = (
  ancestorDocument: ProsemirrorNode,
  currentState: EditorState,
  commit: Commit,
  mapping?: Mapping
): EditorState => {
  // retrieve the current set of annotation in order to map them from old
  // mapping to the new one
  const currentPluginState = getTrackPluginState(currentState)
  const annotations = currentPluginState ? currentPluginState.annotations : []

  // if the passed commit contains steps, then freeze it before allowing
  // more changes
  if (commit.steps.length) {
    commit = freeze(commit)
  }

  const temporaryState = EditorState.create({
    doc: ancestorDocument,
    schema: currentState.schema,
    plugins: currentState.plugins,
    storedMarks: currentState.storedMarks,
  })

  const { tr } = temporaryState

  tr.setMeta(trackPluginKey, {
    type: TRACK_PLUGIN_ACTIONS.REPLACE,
    commit,
    annotations,
  })
  tr.setMeta('addToHistory', false)

  const allSteps = smoosh<Step>(commit, (c) => c.steps)
  // replay all the steps
  allSteps.forEach((step) => tr.maybeStep(step))

  const nextState = temporaryState.apply(tr)

  // remap the selection
  if (mapping && currentState.selection) {
    const { tr: selectionTr } = nextState
    const nextSelection = currentState.selection.map(nextState.doc, mapping)
    selectionTr.setSelection(nextSelection)
    selectionTr.setMeta('addToHistory', false)
    return nextState.apply(selectionTr)
  } else {
    return nextState
  }
}

export const reset = (
  ancestorDocument: ProsemirrorNode,
  currentState: EditorState
) => {
  const { commit } = getTrackPluginState(currentState)
  const { prev, steps } = commit
  const mapping = reverseMapping(steps)
  return checkout(
    ancestorDocument,
    currentState,
    prev || emptyCommit(),
    mapping
  )
}
