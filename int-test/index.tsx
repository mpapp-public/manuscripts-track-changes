/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// @ts-ignore (types)
import { exampleSetup } from 'prosemirror-example-setup'
import { schema } from 'prosemirror-schema-basic'
import { EditorState } from 'prosemirror-state'
import { EditorView } from 'prosemirror-view'
import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import { v4 as uuid } from 'uuid'

import trackPlugin, {
  checkout,
  commands,
  Commit,
  findCommitWithin,
  getTrackPluginState,
  isCommitContiguousWithSelection,
  rebases,
  reset,
} from '../src'
import AnnotationsList from './AnnotationsList'
import CommitsList from './CommitsList'
import ExcludedCommitsList from './ExcludedCommitsList'
import { newDocument } from './io'
import SpriteMap from './SpriteMap'
import useEditor, { CreateView } from './useEditor'

const initialState = EditorState.create({
  doc: newDocument(),
  schema,
  plugins: exampleSetup({ schema }).concat(
    trackPlugin({ ancestorDoc: newDocument(), showBlameSpanButtons: true })
  ),
})

const createView: CreateView = (el, state, dispatch) =>
  new EditorView(el, {
    state,
    dispatchTransaction: dispatch,
  })

const App: React.FC = () => {
  const {
    onRender,
    state,
    doCommand,
    isCommandValid,
    replaceState,
  } = useEditor(initialState, createView)

  const [excluded, setExcluded] = useState<Commit[]>([])

  if (!state) {
    return <div id="editor" ref={onRender}></div>
  }

  const { commit } = getTrackPluginState(state)

  const replayWithout = (without: string[]) => {
    const rejectedCommits = without
      .map(findCommitWithin(commit))
      .filter(Boolean) as Commit[]
    setExcluded((current) => current.concat(rejectedCommits))

    const { commit: next, mapping } = rebases.without(commit, without)
    if (!next) {
      return
    }

    replaceState(checkout(newDocument(), state, next, mapping))
  }

  const replayWith = (pick: Commit) => {
    setExcluded((current) =>
      current.filter((item) => item.changeID !== pick.changeID)
    )

    const { commit: next, mapping } = rebases.cherryPick(pick, commit)
    replaceState(checkout(newDocument(), state, next, mapping))
  }

  const resetToLast = () => {
    replaceState(reset(newDocument(), state))
  }

  console.log(getTrackPluginState(state))

  return (
    <React.Fragment>
      <SpriteMap />
      <div id="editor" ref={onRender}></div>

      <button
        type="button"
        disabled={!isCommandValid(commands.freezeCommit())}
        onClick={() => doCommand(commands.freezeCommit())}
      >
        Group Changes
      </button>

      <button
        type="button"
        disabled={!isCommandValid(commands.addAnnotation('', ''))}
        onClick={() =>
          doCommand(commands.addAnnotation(uuid(), 'rgb(255, 100, 100'))
        }
      >
        Add Annotation
      </button>

      <button
        type="button"
        disabled={!commit.steps.length}
        onClick={resetToLast}
      >
        Reset
      </button>

      <button
        type="button"
        onClick={(e) => {
          e.preventDefault()
          console.log(isCommitContiguousWithSelection(state))
        }}
      >
        Check if contiguous
      </button>

      <CommitsList
        state={state}
        doCommand={doCommand}
        isCommandValid={isCommandValid}
        submit={replayWithout}
      />

      <ExcludedCommitsList excluded={excluded} replayWith={replayWith} />

      <AnnotationsList state={state} doCommand={doCommand} />
    </React.Fragment>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
