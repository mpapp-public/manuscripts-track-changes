/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Command } from 'prosemirror-commands'
import { EditorState } from 'prosemirror-state'
import React from 'react'

import { commands, focusedEntities, getTrackPluginState } from '../src'

interface Props {
  state: EditorState
  doCommand: (command: Command) => void
}

const AnnotationsList: React.FC<Props> = ({ state, doCommand }) => {
  const { annotations } = getTrackPluginState(state)
  const { annotation: focused } = focusedEntities(state)

  return (
    <div>
      <strong>Annotations:</strong>
      <ul>
        {annotations.map((ann) => (
          <li
            key={ann.uid}
            className={
              focused === ann.uid ? 'annotation-focused' : 'annotation'
            }
          >
            <button
              type="button"
              onClick={() => doCommand(commands.removeAnnotation(ann.uid))}
            >
              Remove
            </button>
            <button
              type="button"
              onMouseDown={(e) => {
                e.preventDefault()
                doCommand(commands.focusAnnotation(ann.uid))
                return false
              }}
              onKeyDown={(e) => {
                if ([' ', 'Enter'].includes(e.key)) {
                  doCommand(commands.focusAnnotation(ann.uid))
                }
              }}
            >
              Scroll into view
            </button>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default AnnotationsList
