/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Command } from 'prosemirror-commands'
import { EditorState } from 'prosemirror-state'
import React, { useCallback, useState } from 'react'

import { commands, getChangeSummary, getCommitsList } from '../src'

interface Props {
  state: EditorState
  doCommand: (command: Command) => void
  isCommandValid: (command: Command) => boolean
  submit: (commits: string[]) => void
}

const CommitsList: React.FC<Props> = ({
  state,
  doCommand,
  isCommandValid,
  submit,
}) => {
  const list = getCommitsList(state)

  const [incCommits, setIncCommits] = useState<string[]>([])
  const toggleCommit = useCallback((commit: string) => {
    setIncCommits((commits) => {
      return commits.includes(commit)
        ? commits.filter((c) => c !== commit)
        : commits.concat(commit)
    })
  }, [])

  return (
    <div>
      <strong>Exclude:</strong>
      {list.map(({ _id, changeID }) => {
        return (
          <div className="commit" key={_id}>
            <label>
              <input
                type="checkbox"
                checked={incCommits.includes(changeID)}
                onChange={() => toggleCommit(changeID)}
              />
              {changeID} | {_id}
            </label>
            <button
              type="button"
              disabled={!isCommandValid(commands.focusCommit(changeID))}
              onClick={(e) => {
                e.preventDefault()
                doCommand(commands.focusCommit(changeID))
              }}
            >
              Highlight
            </button>
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault()
                console.log(getChangeSummary(state, changeID))
              }}
            >
              Summarize Changes
            </button>
          </div>
        )
      })}
      <button
        type="button"
        onClick={() => {
          submit(incCommits)
          setIncCommits([])
        }}
      >
        Rewind and Playback
      </button>
    </div>
  )
}

export default CommitsList
