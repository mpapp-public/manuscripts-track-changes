/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Command } from 'prosemirror-commands'
import { EditorState, Transaction } from 'prosemirror-state'
import { EditorView } from 'prosemirror-view'
import { useCallback, useRef, useState } from 'react'

export type CreateView = (
  element: HTMLDivElement,
  state: EditorState,
  dispatch: (tr: Transaction) => void
) => EditorView

interface HookValue {
  state: EditorState
  onRender: (el: HTMLDivElement) => void
  isCommandValid: (command: Command) => boolean
  doCommand: (command: Command) => boolean
  replaceState: (state: EditorState) => void
  dispatch: (tr: Transaction) => void
  view?: EditorView
}

const useEditor = (
  initialState: EditorState,
  createView: CreateView
): HookValue => {
  const view = useRef<EditorView>()
  const [state, setState] = useState<EditorState>(initialState)

  const dispatch = (tr: Transaction) => {
    if (!view.current) {
      return
    }

    const nextState = view.current.state.apply(tr)
    view.current.updateState(nextState)

    // TODO: this part should be debounced??
    setState(nextState)
  }

  const onRender = useCallback((el: HTMLDivElement | null) => {
    if (!el) {
      return
    }

    view.current = createView(el, state, dispatch)
    setState(view.current.state)
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const isCommandValid = useCallback(
    (command: Command): boolean => command(state),
    [state]
  )

  const doCommand = useCallback(
    (command: Command): boolean => command(state, dispatch, view.current),
    [state]
  )

  const replaceState = useCallback((state: EditorState) => {
    setState(state)
    if (view.current) {
      view.current.updateState(state)
    }
  }, [])

  return {
    // ordinary use:
    state,
    onRender,
    isCommandValid,
    doCommand,
    replaceState,

    // advanced use:
    view: view.current,
    dispatch,
  }
}

export default useEditor

interface FaccProps {
  initialState: EditorState
  createView: CreateView
  children: (hookValue: HookValue) => JSX.Element
}

export const EditorStateComponent: React.FC<FaccProps> = ({
  initialState,
  createView,
  children,
}) => {
  const editorProps = useEditor(initialState, createView)

  return children(editorProps)
}
